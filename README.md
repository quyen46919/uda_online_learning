# Uda Online Learning
## See project production
https://uda-forum-fe.vercel.app/

## Install packages
``yarn install`` or ```npm install```

## Run app
```yarn dev``` or ```npm run dev```

## Build app
```yarn build``` or ```npm run build```

## Graphql MUI NextJS Boilerplate
A boilerplate/starter project for quickly building NextJS app using React, Mui, Redux, RTK query and GraphQL, GITLAB CICD and auto deploy with Vercel

## UI Template
Figma: https://www.figma.com/file/zocFgkCPrZ1KTlBzdXe3xf/UDA-Forum-Template?node-id=0%3A1&t=s8E42umHjpN7AhN0-0<br/>
Dribbble: https://dribbble.com/shots/18325006-Forum-Web-App-Design